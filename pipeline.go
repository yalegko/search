package pipeline

import (
	"bytes"
	"crypto/tls"
	"log"
	"sync"
	"sync/atomic"

	"github.com/valyala/fasthttp"
)

var bytesPool = sync.Pool{
	New: func() interface{} { return make([]byte, 4096) },
}

func SearchWord(urls <-chan string, word string) int {
	var res uint64

	// Reading the description we could suppose that it's CPU bounded and we should
	// use a worker pool bounded to NumCPUs, but it's actually an IO bounded so we
	// just need as many goroutines as we can.
	var wg sync.WaitGroup
	for i := 0; i < len(urls); i++ {
		wg.Add(1)
		go worker(&wg, urls, []byte(word), &res)
	}

	wg.Wait()
	return int(res)
}

func worker(wg *sync.WaitGroup, urls <-chan string, word []byte, res *uint64) {
	// Reuse fasthttp.Client. We could balance requests by host here, but it
	// looks a bit cheaty.
	client := &fasthttp.Client{}
	client.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
	}

	// Again we are going to use atomic as a result holder just cos it's more
	// easy.
	for u := range urls {
		counted := countURL(client, u, word)
		atomic.AddUint64(res, uint64(counted))
	}

	wg.Done()
}

func countURL(client *fasthttp.Client, url string, matcher []byte) int {
	// A bit of "real world" optimizations for buffer reuse.
	buf := bytesPool.Get().([]byte)

	_, body, err := client.Get(buf, url)
	if err != nil {
		log.Fatalf("Failed to GET %q: %s\n", url, err)
	}
	res := bytes.Count(body, matcher)

	bytesPool.Put(body)
	return res
}
